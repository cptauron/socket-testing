module.exports = function (socket) {
    console.log('a user connected');

var accounts = [
    {
        "account": "Brad",
        "characters": [
            {
                "name": "Brad",
                "level": 1,
                "class": "Wizard",
                "stats": {
                    "strength": 5,
                    "intelligence": 5
                }
            }
        ]
    }
];

socket.on('character:get:bio', function (userid) {
    var temp = accounts[Number(userid)];
    console.log('ID: ' + userid);
    socket.emit('character:bio', temp);
});

socket.on('disconnect', function () {
    console.log('user disconnected');
});
}
;