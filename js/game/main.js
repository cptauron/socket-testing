angular.module('socketGame', [
    'socketGame.controllers',
    'socketGame.sockets',
    'socketGame.service',

    //socket-io wrapper factory
    'btford.socket-io'
]);

//Services Module
angular.module('socketGame.sockets', []).
    factory('socket', function (socketFactory) {
        return socketFactory();
    });

angular.module('socketGame.service', []).
    factory('service', function () {
        var account = {};

        return {
            setAccount: function(data) {
              account = data;
            },
            getAccount: function() {
                return account;
            }
        }
    });

//Controllers module
angular.module('socketGame.controllers',[]).
    controller('mainController', ['$scope', 'socket', 'service', function($scope, socket, service) {
        $scope.window = {};
        $scope.window.location = "account:login";
        $scope.window.stats = false;
        $scope.window.charselect = false;
        $scope.window.login = true;
        $scope.character = {};


        socket.on('character:bio', function (data) {
            $scope.character.bio = data;
            console.log($scope.character.bio);
        });

        $scope.login = function() {
            console.log("Login!");
            $scope.getBio(0);

            $scope.window.location = "character:select";
            $scope.window.login = false;
            $scope.window.stats = false;
            $scope.window.charselect = true;
        };

        $scope.getBio = function (playerid) {
            socket.emit('character:get:bio', playerid);
        };
    }]);

/*
    Character Creation:
    get options eg. appearance, class,
    set options

 */