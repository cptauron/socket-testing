var   express  = require('express')
,     app      = express()
,     http     = require('http').Server(app)
,     io       = require('socket.io')(http)
,     jade     = require('jade');

app.set('env', 'development');

if (app.get('env') === 'development') {
   app.locals.pretty = true;
   app.use('/js/game', express.static(__dirname + '/js/game'));
   app.use('/bower_components', express.static(__dirname + '/bower_components'));
}


app.set('port', (process.env.PORT || 3000));
app.set('views', __dirname + '/jade');

app.get('/', function(req, res) {
   console.log("You have great success");
   res.render('home.jade');
});

app.get('/game', function(req, res) {
   console.log("You have great success");
   res.render('index.jade');
});

io.sockets.on('connection', require('./sockets/sockets'));

http.listen(app.get('port'), function () {
   console.log("App listening on port " + app.get('port'));
});